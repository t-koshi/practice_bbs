package domain.Repository

import domain.model.Text

class TextRepositoryImpl extends TextRepository{
  /**
   * 指定したユーザーのテキストの編集内容を保存する
   * @param text 編集するテキスト
   * @return 編集後のテキスト
   */
  override def update(text: Text): Text = ???

  /**
   * ユーザーが書き込んだテキストを保存する
   * @param text ユーザーが書き込んだテキスト
   * @return ユーザーが書き込んだテキスト
   */
  override def insert(text: Text): Text = ???

  /**
   * 投稿一覧を取得
   * @return ユーザーの書き込み一覧
   */
  override def selectAll(): Seq[Text] = ???
}
