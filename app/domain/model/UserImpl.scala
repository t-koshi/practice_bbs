package domain.model

/**
 * Created by greapflute on 15/03/07.
 */
class UserImpl(val id: Integer) extends User{

  override def modifyText(beforeText: Text, text: String): Text = {
    new TextImpl(beforeText.id, text, beforeText.userId, beforeText.createdAt)
  }
}
