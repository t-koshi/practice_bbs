package controllers

import java.sql.Timestamp

import domain.Repository.TextRepositoryImpl
import domain.model.TextImpl
import play.api.mvc._

object Application extends Controller {

  def index = Action {

    /**
     * 書き込み処理
     */
    val id = 1
    val userId = 1
    val createdAt = new Timestamp(System.currentTimeMillis())
    val writeText = new TextImpl(id, "書き込まれたテキスト", userId, createdAt)
    new TextRepositoryImpl().insert(writeText)

    Ok(views.html.index("Your new application is ready."))
  }

}